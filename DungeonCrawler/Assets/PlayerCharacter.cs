﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerCharacter : MonoBehaviour, ITakesDamage
{
    public int HitPoints = 100;
    public int GoldPieces = 0;
    public float AttackRange = 10f;
    public bool IsAlive = true;

    private NavMeshAgent _agent;
    private Animator _animator;
    public GameObject EnemyToAttack;

    public int RemainingHitPoints()
    {
        return HitPoints;
    }

    // Use this for initialization
    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetButton("Jump"))
        {
            Debug.Log("Swing!");
            Attack();
            _animator.SetTrigger("WeaponSwing");
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Enemy set!");
        if (other.gameObject.tag == "Enemy")
        {
            EnemyToAttack = other.gameObject;
        }
    }
    public void Attack()
    {
        //get the closest enemy
        if (EnemyToAttack != null)
        {
            Debug.Log(EnemyToAttack.name + " is " + _agent.remainingDistance + " away.");
            var distanceToEnemy =
                Mathf.Abs(Vector3.Distance(gameObject.transform.position, EnemyToAttack.transform.position));

            if (distanceToEnemy < AttackRange)
            {
                var damage = RollForHit();
                //if we did any damage, let's damage the enemy
                if (damage > 0)
                {
                    var damageScript = EnemyToAttack.GetComponent<ITakesDamage>();
                    damageScript.TakeDamage(damage);
                    Debug.Log("Player hits for " + damage + " damage.  " + damageScript.RemainingHitPoints() +
                              " remain.");
                }
            }
            else
            {
                Debug.Log("You are too far away to hit the enemy.");
            }
        }
    }

    private int RollForHit()
    {
        //roll a twenty sided die to see if you hit
        var attackRoll = Random.Range(1, 20);
        var damage = 0;
        //keeping it simple, we'll give the hero a 50/50 chance to hit
        //If you wanted to make something better, pass in the game object of the opponent
        //and attach some construct for armor class, level, etc.
        if (attackRoll > 9)
        {
            //hit!  how much damage
            // again you'd probably want the opponent's AC at a minimum
            damage = Random.Range(1, 10);
        }
        Debug.Log("You rolled " + damage + " damage.");
        return damage;
    }

    public void GetTreasure(int howMuch)
    {
        GoldPieces += howMuch;
    }

    public void TakeDamage(int howMuch)
    {
        var newHitPoints = HitPoints - howMuch;
        if (newHitPoints < 0)
        {
            HitPoints = 0;
            IsAlive = false;
            _agent.Stop();
            _animator.SetBool("IsDead", true);
        }
        else
        {
            HitPoints = newHitPoints;
        }
    }
}
