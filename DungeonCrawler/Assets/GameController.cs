﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text PlayerHealthDisplay;
    public Slider PlayerHealthMeter;
            
    public Text EnemyHealthDisplay;
    public Slider EnemyHealthMeter;

    public Text GoldDisplay;

    public GameObject Player;
    public GameObject EnemyPrefab;
    private GameObject _currentEnemy;
    public bool TreasureFound = false;

    // Use this for initialization
	void Start ()
	{
	    //spawn the first enemy
        SpawnEnemy();
	}
	
	// Update is called once per frame
	void Update ()
	{
	   GoldDisplay.text = Player.GetComponent<PlayerCharacter>().GoldPieces.ToString();

	   PlayerHealthDisplay.text = Player.GetComponent<PlayerCharacter>().RemainingHitPoints().ToString();
	   PlayerHealthMeter.maxValue = Player.GetComponent<PlayerCharacter>().HitPoints;
	   PlayerHealthMeter.value = Player.GetComponent<PlayerCharacter>().RemainingHitPoints();

	    EnemyHealthDisplay.text = _currentEnemy.GetComponent<MonsterController>().HitPoints.ToString();
	    EnemyHealthMeter.maxValue = _currentEnemy.GetComponent<MonsterController>().MaxHitPoints;
	    EnemyHealthMeter.value = _currentEnemy.GetComponent<MonsterController>().HitPoints;

        var script = _currentEnemy.GetComponent<MonsterController>();
        if (script.IsDead)
        {
            Destroy(_currentEnemy, 2);
            SpawnEnemy();
        }
    }

    private void SpawnEnemy()
    {
        //get a random patrol point
        var patrolPoints = GameObject.FindGameObjectsWithTag("PatrolPoint");
        int randomIndex = Random.Range(0, patrolPoints.Length);
        var spawnPosition = patrolPoints[randomIndex].transform.position;
        _currentEnemy = (GameObject) Instantiate(EnemyPrefab, spawnPosition, Quaternion.identity);
        _currentEnemy.layer = LayerMask.NameToLayer("Player");
    }
}
