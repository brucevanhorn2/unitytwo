﻿using UnityEngine;
using System.Collections;

public class GetTreasure : MonoBehaviour
{
    public int TreasureValue = 100;
    public AudioClip TreasureSound;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //get the player character script
            var script = other.gameObject.GetComponent<PlayerCharacter>();
            script.GetTreasure(TreasureValue);
            
            //play the audio clip
            var cameraAudio = Camera.main.GetComponent<AudioSource>();
            cameraAudio.clip = TreasureSound;
            cameraAudio.Play();

            Destroy(gameObject, .5f);
            var gc = GameObject.FindGameObjectWithTag("GameController");
            gc.GetComponent<GameController>().TreasureFound = true; //This ends the game
            Invoke("GameOver", 1f);

        }
    }

    public void GameOver() { Application.LoadLevel("GameOver"); }
}
