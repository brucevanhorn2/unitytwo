﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour
{
    public float Speed = 10f;
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up, Speed * Time.deltaTime);
    }
}
