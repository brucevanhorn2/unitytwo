﻿using UnityEngine;
using System.Collections;

public class ClickTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Click!");
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000))
            {

                //if you click on an enemy, you are attacking them and they
                //become the destination.  Otherwise, just go where we clicked.

                //I think it's too precise... maybe if the enemy is within a few meters of wherever you clicked?
                var distanceToEnemy = Mathf.Abs(Vector3.Distance(hit.point,
                    GameObject.FindGameObjectWithTag("Enemy").transform.position));

                if (distanceToEnemy < 50)
                {
                    Debug.Log("You clicked an enemy.");
                   
                }
                else
                {
                    
                    Debug.Log("You clicked a point on the map.");
                }
                
            }

        }
    }
}
