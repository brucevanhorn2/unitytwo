﻿using UnityEngine;
using System.Collections;

public class FollowCam : MonoBehaviour {
	public GameObject Player;
	public Camera MapCamera;

	private Vector3 _offset;
	private Vector3 _wheelDistance;
	// Use this for initialization
	void Start () {
		_offset = transform.position - Player.transform.position;
		_wheelDistance = new Vector3 (0.0f, 0.5f, 0.5f);
	}
	
	// Update is called once per frame
	void Update(){

	}
	void LateUpdate () {

		if (Input.GetKeyUp (KeyCode.M)) {
			//Toggle the map cam
			MapCamera.enabled = !MapCamera.enabled;
		}
		var d = Input.GetAxis("Mouse ScrollWheel");
		if (d > 0f)
		{
			_offset = (transform.position - Player.transform.position) - _wheelDistance;
		}
		else if (d < 0f)
		{
			_offset = (transform.position - Player.transform.position) + _wheelDistance;
		}
		transform.position = Player.transform.position + _offset;
	}
}
