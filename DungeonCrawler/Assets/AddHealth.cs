﻿using UnityEngine;
using System.Collections;

public class AddHealth : MonoBehaviour
{
    public int PointsToRestore = 10;
    public AudioClip HealthSound;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //get the player character script
            var script = other.gameObject.GetComponent<PlayerCharacter>();
            //use the take damage method and pass it a negative value
            script.TakeDamage(PointsToRestore * -1);
            
            //play the audio clip
            var cameraAudio = Camera.main.GetComponent<AudioSource>();
            cameraAudio.clip = HealthSound;
            cameraAudio.Play();

            Destroy(gameObject, .5f);

        }
    }
}
