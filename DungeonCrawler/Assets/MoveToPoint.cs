﻿using UnityEngine;
using System.Collections;

public class MoveToPoint : MonoBehaviour {

	NavMeshAgent agent;
	private Animator _animator;

	void Start() {
		agent = GetComponent<NavMeshAgent>();
		agent.SetDestination (transform.position);
		_animator = GetComponent<Animator> ();
	}
	
	void Update() {
        if (agent.destination != null)
        {
            var distanceToGoal = Vector3.Distance(transform.position, agent.destination);
            if (distanceToGoal < 0.2)
            {
                //stop walking
                _animator.SetFloat("Speed", 0);
            }
        }
        if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;
            var characterController = gameObject.GetComponent<PlayerCharacter>();
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000)) {

                //if you click on an enemy, you are attacking them and they
                //become the destination.  Otherwise, just go where we clicked.

                //I think it's too precise... maybe if the enemy is within a few meters of wherever you clicked?
                var distanceToEnemy = Mathf.Abs(Vector3.Distance(hit.point,
                    GameObject.FindGameObjectWithTag("Enemy").transform.position));

			    if (distanceToEnemy < 50)
			    {
                    Debug.Log("You clicked an enemy.");
			        agent.destination = hit.collider.gameObject.transform.position;
			        characterController = gameObject.GetComponent<PlayerCharacter>();
			        characterController.EnemyToAttack = hit.collider.gameObject;
			    }
			    else
			    {
                    agent.destination = hit.point;
			        characterController.EnemyToAttack = null;
                    Debug.Log("You clicked a point on the map.");
                }
                _animator.SetFloat("Speed", 1);
            }
            
		}
	}
}
