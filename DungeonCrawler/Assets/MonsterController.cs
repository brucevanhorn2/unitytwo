﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class MonsterController : MonoBehaviour, ITakesDamage
{
    
	public float AttackRange = 0.2f;
    public int MaxHitPoints = 32;
	public int HitPoints = 32;
	public int DamagePerHitUpperLimit = 18;
	public float PercentHit = 70f;
    public RectTransform EnemyDisplay;
	private NavMeshAgent _agent;
	private Animator _animator;
	public bool _awareOfPlayerPosition;
    public bool IsDead = false;

    public int RemainingHitPoints()
    {
        return HitPoints;
    }
	// Use this for initialization
	void Start () {
		_awareOfPlayerPosition = false;
		_agent = GetComponent<NavMeshAgent>();
		_animator = GetComponent<Animator> ();
		_animator.SetFloat ("Speed", 1.0f);
	    SetNewDestination();
	}

	void OnTriggerEnter(Collider other){

		if (other.gameObject.tag == "Player") {
			//player detected
			Debug.Log ("Collision detected");
			_awareOfPlayerPosition = true;
			_agent.SetDestination(other.gameObject.transform.position);
			_agent.speed = 5f;
			_animator.SetFloat("Speed", 2.0f);

		    var playerCharacter = other.gameObject.GetComponent<PlayerCharacter>();
		    playerCharacter.EnemyToAttack = this.gameObject;
		}

	    
	}

	// Update is called once per frame
	void Update () {
		//raycast to determine attack range
	    if (_awareOfPlayerPosition)
	    {
	        var player = GameObject.FindGameObjectWithTag("Player");
	        var distance = Mathf.Abs(Vector3.Distance(player.transform.position, transform.position));
	        if (distance < AttackRange)
	        {
	            _animator.SetBool("IsAttacking", true);
	        }

	    }
	    else
	    {
           
            //find the distance to the next patrol point
            if (_animator.GetFloat("Speed") > 0 && _agent.remainingDistance < 1.0f)
	        {
	            _agent.Stop();
                _animator.SetFloat("Speed", 0.0f);

	            StartCoroutine(PatrolPause());

	        }
	    }
	}

    private IEnumerator PatrolPause()
    {
        //wait zero to 7 seconds before resuming patrol
        yield return new WaitForSeconds(Random.Range(0,7));
        SetNewDestination();
    }
    private void SetNewDestination()
    {
        //find a new destination
        var possibleDestinations = GameObject.FindGameObjectsWithTag("PatrolPoint");
        var randomIndex = Random.Range(0, possibleDestinations.Count());
        var newDestination = possibleDestinations[randomIndex].transform.position;
        Debug.Log("New patrol point: " + possibleDestinations[randomIndex].name);
        _agent.SetDestination(newDestination);
        _agent.Resume();
        _animator.SetFloat("Speed", 1.0f);
    }

    public void TakeDamage(int howMuch)
    {
        var newHitPoints = HitPoints - howMuch;
        if (newHitPoints < 0)
        {
            IsDead = true;
            _agent.Stop();
            _animator.SetBool("IsDead", true);
        }
        else
        {
            HitPoints = newHitPoints;
            Debug.Log("EnemyPrefab received " + howMuch + " HP damage.  " + newHitPoints + " remain.");
            if (howMuch > 0)
            {
                _animator.SetTrigger("Ouch");
            }
            else
            {
                _animator.SetTrigger("Block");
            }
        }
    }
}
