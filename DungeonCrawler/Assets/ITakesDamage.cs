﻿public interface ITakesDamage
{
    void TakeDamage(int howMuch);
    int RemainingHitPoints();
}
