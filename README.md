# Unity TWO Instructor Code #

This repo contains the code built for Unity TWO at Richland College by Bruce Van Horn, Adjunct Professor, Game Development

It is not open to contributions.  It is here to serve as the basis for classroom instruction.

You are not granted any rights to use this code outside of classroom instruction.